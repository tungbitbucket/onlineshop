﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ClassLibrary
{
    public static class CommonFunction
    {
        public static void SetSession(string key, dynamic value)
        {
            HttpContext.Current.Session[key] = value;
        }

        public static dynamic GetSession(string key)
        {
            return HttpContext.Current.Session[key];
        }

        public static DateTime CurrentTimeForDB()
        {
            var src = DateTime.Now;
            return new DateTime(src.Year, src.Month, src.Day, src.Hour, src.Minute, 0);
        }
    }
}
