namespace Models.Framework
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderItem")]
    public partial class OrderItem
    {
        [Key]
        [Column("OrderItem")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderItem1 { get; set; }

        public int? OrderID { get; set; }

        public int? ProductID { get; set; }

        public int? OrderQuantity { get; set; }
    }
}
