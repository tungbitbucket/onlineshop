namespace Models.Framework
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderID { get; set; }

        public int? Status { get; set; }

        [StringLength(100)]
        public string Memo { get; set; }

        [StringLength(100)]
        public string ShippingFirstName { get; set; }

        [StringLength(100)]
        public string ShippingLastName { get; set; }

        [StringLength(300)]
        public string ShippingAddress1 { get; set; }

        [StringLength(300)]
        public string ShippingAddress2 { get; set; }

        [StringLength(100)]
        public string ShippingPhone { get; set; }

        [StringLength(100)]
        public string ShippingState { get; set; }

        [StringLength(100)]
        public string ShippingCity { get; set; }

        [StringLength(50)]
        public string ShippingPostalCode { get; set; }

        public double? ShippingCost { get; set; }

        [StringLength(100)]
        public string PaymentFirstName { get; set; }

        [StringLength(100)]
        public string PaymentLastName { get; set; }

        [StringLength(300)]
        public string PaymentAddress1 { get; set; }

        [StringLength(300)]
        public string PaymentAddress2 { get; set; }

        [StringLength(100)]
        public string PaymentPhone { get; set; }

        [StringLength(100)]
        public string PaymentCity { get; set; }

        [StringLength(100)]
        public string PaymentState { get; set; }

        [StringLength(50)]
        public string PaymentPostalCode { get; set; }
    }
}
