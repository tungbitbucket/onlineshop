namespace Models.Framework
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Currency")]
    public partial class Currency
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CurrencyID { get; set; }

        [StringLength(300)]
        public string CurrencyCode { get; set; }

        [StringLength(300)]
        public string CurrencySymbol { get; set; }
    }
}
