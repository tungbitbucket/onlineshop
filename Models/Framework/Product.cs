namespace Models.Framework
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        [Key]
        public int ProductID { get; set; }

        [StringLength(300)]
        public string GroupProductID { get; set; }

        public int? SupplierID { get; set; }

        [StringLength(300)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        public double? Price { get; set; }

        public int? Quantity { get; set; }

        [StringLength(300)]
        public string ImgUrl { get; set; }

        [StringLength(300)]
        public string ThumnailUrl { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        [NotMapped]
        public int[] ArrayGroupProductID { get; set; }
    }
}
