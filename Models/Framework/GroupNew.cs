namespace Models.Framework
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GroupNew
    {
        [Key]
        public int GroupNewsID { get; set; }

        public int? ParentID { get; set; }

        [StringLength(300)]
        [Required]
        public string Name { get; set; }

        [StringLength(300)]
        public string Detail { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }
    }
}
