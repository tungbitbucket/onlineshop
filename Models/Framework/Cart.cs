namespace Models.Framework
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cart")]
    public partial class Cart
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CartID { get; set; }

        public int? ProductID { get; set; }

        public Guid? SessionID { get; set; }

        public int? Quantity { get; set; }

        public DateTime? Date { get; set; }
    }
}
