﻿using ClassLibrary;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ProductModel
    {
        public OnlineShopDbContext context = null;

        public ProductModel()
        {
            context = new OnlineShopDbContext();
        }

        public List<Product> ListAll()
        {
            return context.Products.ToList();
        }

        public Product ViewDetail(int NewsID)
        {
            return context.Products.Find(NewsID);
        }


        public int Create(Product collection)
        {
            try
            {
                Product ProductToAdd = new Product();
                ProductToAdd.GroupProductID = collection.GroupProductID;
                ProductToAdd.SupplierID = collection.SupplierID;
                ProductToAdd.Name = collection.Name;
                ProductToAdd.Description = collection.Description;
                ProductToAdd.Price = collection.Price;
                ProductToAdd.ImgUrl = collection.ImgUrl;
                ProductToAdd.Quantity = collection.Quantity;
                ProductToAdd.ThumnailUrl = collection.ThumnailUrl;
                ProductToAdd.CreatedDate = CommonFunction.CurrentTimeForDB();
                ProductToAdd.CreatedBy = collection.CreatedBy;
                ProductToAdd.ModifiedDate = CommonFunction.CurrentTimeForDB();
                ProductToAdd.ModifiedBy = collection.CreatedBy;
                context.Products.Add(ProductToAdd);
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public int Edit(Product collection, int ID)
        {
            try
            {
                Product ProductToEdit = context.Products.SingleOrDefault(n => n.ProductID == ID);
                ProductToEdit.GroupProductID = collection.GroupProductID;
                ProductToEdit.SupplierID = collection.SupplierID;
                ProductToEdit.Name = collection.Name;
                ProductToEdit.Description = collection.Description;
                ProductToEdit.Price = collection.Price;
                ProductToEdit.ImgUrl = collection.ImgUrl;
                ProductToEdit.Quantity = collection.Quantity;
                ProductToEdit.ThumnailUrl = collection.ThumnailUrl;
                ProductToEdit.CreatedDate = CommonFunction.CurrentTimeForDB();
                ProductToEdit.CreatedBy = collection.CreatedBy;
                ProductToEdit.ModifiedDate = CommonFunction.CurrentTimeForDB();
                ProductToEdit.ModifiedBy = collection.CreatedBy;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Delete(int ProductID)
        {
            Product ProductToDelete = new Product() { ProductID = ProductID };
            context.Products.Attach(ProductToDelete);
            context.Products.Remove(ProductToDelete);
            return context.SaveChanges();
        }
    }
}
