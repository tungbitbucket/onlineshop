﻿using ClassLibrary;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class GroupNewsModel
    {
        public OnlineShopDbContext context = null;
        public GroupNewsModel()
        {
            context = new OnlineShopDbContext();
        }

        public List<GroupNew> List()
        {
            return context.GroupNews.ToList();
        }

        public int Create(GroupNew collection)
        {
            GroupNew GroupNewsToAdd = new GroupNew();
            GroupNewsToAdd.ParentID = collection.ParentID;
            GroupNewsToAdd.Name = collection.Name;
            GroupNewsToAdd.Detail = collection.Detail;
            GroupNewsToAdd.CreatedDate = CommonFunction.CurrentTimeForDB();
            GroupNewsToAdd.ModifiedDate = CommonFunction.CurrentTimeForDB();
            GroupNewsToAdd.CreatedBy = collection.CreatedBy;
            GroupNewsToAdd.ModifiedBy = collection.ModifiedBy;
            context.GroupNews.Add(GroupNewsToAdd);
            return context.SaveChanges();
        }

        public int Delete(int GroupNewsID)
        {
            GroupNew GroupNewsToDelete = new GroupNew() { GroupNewsID = GroupNewsID };
            context.GroupNews.Attach(GroupNewsToDelete);
            context.GroupNews.Remove(GroupNewsToDelete);
            return context.SaveChanges();
        }
        public GroupNew ViewDetail(int GroupNewsID)
        {
            return context.GroupNews.Find(GroupNewsID);
        }
        public int Edit(GroupNew collection, int ID)
        {
            var GroupNewToUpdate = context.GroupNews.SingleOrDefault(g => g.GroupNewsID == ID);
            GroupNewToUpdate.ParentID = collection.ParentID;
            GroupNewToUpdate.Name = collection.Name;
            GroupNewToUpdate.Detail = collection.Detail;
            GroupNewToUpdate.ModifiedDate = CommonFunction.CurrentTimeForDB();
            GroupNewToUpdate.ModifiedBy = collection.ModifiedBy;
            return context.SaveChanges();
        }
    }
}
