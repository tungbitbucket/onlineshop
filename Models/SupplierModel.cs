﻿using ClassLibrary;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class SupplierModel
    {
        public OnlineShopDbContext context = null;

        public SupplierModel()
        {
            context = new OnlineShopDbContext();
        }

        public List<Supplier> ListAll()
        {
            return context.Suppliers.ToList();
        }

        public Supplier ViewDetail(int NewsID)
        {
            return context.Suppliers.Find(NewsID);
        }


        public int Create(Supplier collection)
        {
            try
            {
                Supplier SupplierToAdd = new Supplier();
                SupplierToAdd.Name = collection.Name;
                SupplierToAdd.Address = collection.Address;
                SupplierToAdd.Phone = collection.Phone;
                SupplierToAdd.Birthday = collection.Birthday;
                SupplierToAdd.Email = collection.Email;
                context.Suppliers.Add(SupplierToAdd);
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public int Edit(Supplier collection, int ID)
        {
            try
            {
                var SupplierToEdit = context.Suppliers.SingleOrDefault(n => n.SupplierID == ID);
                SupplierToEdit.Name = collection.Name;
                SupplierToEdit.Address = collection.Address;
                SupplierToEdit.Phone = collection.Phone;
                SupplierToEdit.Birthday = collection.Birthday;
                SupplierToEdit.Email = collection.Email;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public int Delete(int SupplierID)
        {
            Supplier SupplierToDelete = new Supplier() { SupplierID = SupplierID };
            context.Suppliers.Attach(SupplierToDelete);
            context.Suppliers.Remove(SupplierToDelete);
            return context.SaveChanges();
        }
    }
}
