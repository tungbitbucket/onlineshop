﻿using ClassLibrary;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class NewsModel
    {
        public OnlineShopDbContext context = null;
      
        public NewsModel()
        {
            context = new OnlineShopDbContext();
        }

        public List<News> ListAll()
        {
            return context.News.ToList();
        }

        public News ViewDetail(int NewsID)
        {
            return context.News.Find(NewsID);
        }


        public int Create(News collection)
        {
            try
            {
                News NewsToAdd = new News();
                NewsToAdd.Title = collection.Title;
                NewsToAdd.GroupNewsID = collection.GroupNewsID;
                NewsToAdd.Content = collection.Content;
                NewsToAdd.CreatedDate = CommonFunction.CurrentTimeForDB();
                NewsToAdd.CreatedBy = collection.CreatedBy;
                NewsToAdd.ModifiedDate = CommonFunction.CurrentTimeForDB();
                NewsToAdd.ModifiedBy = collection.CreatedBy;
                context.News.Add(NewsToAdd);
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public int Edit(string GroupNewsTitle, string GroupNewsContent, string ModifiedBy, int ID)
        {
            News NewsToUpdate = context.News.SingleOrDefault(n => n.NewsID == ID);
            NewsToUpdate.Title = GroupNewsTitle;
            NewsToUpdate.Content = GroupNewsContent;
            NewsToUpdate.ModifiedDate = CommonFunction.CurrentTimeForDB();
            NewsToUpdate.ModifiedBy = ModifiedBy;
            return context.SaveChanges();
        }

        public int Edit(News collection, int ID)
        {
            try
            {
                var NewsToUpdate = context.News.SingleOrDefault(n => n.NewsID == ID);
                NewsToUpdate.Title = collection.Title;
                NewsToUpdate.GroupNewsID = collection.GroupNewsID;
                NewsToUpdate.Content = collection.Content;
                NewsToUpdate.ModifiedDate = CommonFunction.CurrentTimeForDB();
                NewsToUpdate.ModifiedBy = collection.ModifiedBy;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Delete(int NewsID)
        {
            News NewsToDelete = new News() { NewsID = NewsID };
            context.News.Attach(NewsToDelete);
            context.News.Remove(NewsToDelete);
            return context.SaveChanges();
        }
    }
}