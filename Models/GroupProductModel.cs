﻿using ClassLibrary;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class GroupProductsModel
    {
        public OnlineShopDbContext context = null;
        public GroupProductsModel()
        {
            context = new OnlineShopDbContext();
        }

        public List<GroupProduct> List()
        {
            return context.GroupProducts.ToList();
        }

        public int Create(GroupProduct collection)
        {
            GroupProduct GroupProductsToAdd = new GroupProduct();
            GroupProductsToAdd.ParentID = collection.ParentID;
            GroupProductsToAdd.Name = collection.Name;
            GroupProductsToAdd.Detail = collection.Detail;
            GroupProductsToAdd.CreatedDate = CommonFunction.CurrentTimeForDB();
            GroupProductsToAdd.ModifiedDate = CommonFunction.CurrentTimeForDB();
            GroupProductsToAdd.CreatedBy = collection.CreatedBy;
            GroupProductsToAdd.ModifiedBy = collection.ModifiedBy;
            context.GroupProducts.Add(GroupProductsToAdd);
            return context.SaveChanges();
        }

        public int Delete(int GroupProductsID)
        {
            GroupProduct GroupProductsToDelete = new GroupProduct() { GroupProductID = GroupProductsID };
            context.GroupProducts.Attach(GroupProductsToDelete);
            context.GroupProducts.Remove(GroupProductsToDelete);
            return context.SaveChanges();
        }
        public GroupProduct ViewDetail(int GroupProductsID)
        {
            return context.GroupProducts.Find(GroupProductsID);
        }
        public int Edit(GroupProduct collection, int ID)
        {
            var GroupProductToUpdate = context.GroupProducts.SingleOrDefault(g => g.GroupProductID == ID);
            GroupProductToUpdate.ParentID = collection.ParentID;
            GroupProductToUpdate.Name = collection.Name;
            GroupProductToUpdate.Detail = collection.Detail;
            GroupProductToUpdate.ModifiedDate = CommonFunction.CurrentTimeForDB();
            GroupProductToUpdate.ModifiedBy = collection.ModifiedBy;
            return context.SaveChanges();
        }
    }
}
