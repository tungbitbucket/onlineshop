﻿using Models.Framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class AccountModel
    {
        public OnlineShopDbContext context = null;
        public AccountModel()
        {
            context = new OnlineShopDbContext();
        }
        public bool Login(string Username, string Password)
        {
            try
            {
                object[] SqlParams = { new SqlParameter("@Username", Username), new SqlParameter("@Password", Password) };
                return context.Database.SqlQuery<int>("sproc_AdminLogin @Username, @Password", SqlParams).SingleOrDefault() == 1;
            }
            catch (Exception ex)
            {
                return false;
            }    
        }

        public SysAccount ViewDetailByUsername(string Username)
        {
            return context.SysAccounts.Where(a => a.Username == Username).SingleOrDefault();
        }
    }
}
