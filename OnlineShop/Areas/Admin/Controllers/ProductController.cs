﻿using ClassLibrary;
using Models;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        private ProductModel ProductModel = new ProductModel();
        private GroupProductsModel GroupProductsModel = new GroupProductsModel();
        private SupplierModel SupplierModel = new SupplierModel();

        public ActionResult Index()
        {
            return View(ProductModel.ListAll());
        }

        public ActionResult Create()
        {
            ViewBag.SupplierID = SupplierModel.ListAll();
            ViewBag.GroupProductList = GroupProductsModel.List();
            return View();
        }

        [Authorize]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product collection)
        {
            collection.GroupProductID = string.Join(",", collection.ArrayGroupProductID);
            collection.CreatedBy = CommonFunction.GetSession("AccountLogin");
            if (ProductModel.Create(collection) == 1)
            {
                TempData["SuccesStatus"] = "Create Success!";
                return RedirectToAction("Create");
            }
            TempData["ErrorStatus"] = "Create Fail!";
            return View(collection);
        }

        public ActionResult Delete(int ID)
        {
            if (ProductModel.Delete(ID) == 1)
            {
                TempData["SuccessStatus"] = "Delete Success!";
                return RedirectToAction("Index");
            }
            TempData["ErrorStatus"] = "Delete Fail!";
            return View();
        }

        public ActionResult Edit(int ID)
        {
            ViewBag.SupplierID = SupplierModel.ListAll();
            var ViewDetail = ProductModel.ViewDetail(ID);
            ViewBag.GroupProductList = GroupProductsModel.List();
            return View(ViewDetail);
        }

        [Authorize]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product collection, int ID)
        {
            collection.GroupProductID = string.Join(",", collection.ArrayGroupProductID);
            collection.ModifiedBy = CommonFunction.GetSession("AccountLogin");
            if (ProductModel.Edit(collection, ID) == 1)
            {
                TempData["SuccesStatus"] = "Update Success!";
                return RedirectToAction("Edit");
            }
            TempData["ErrorStatus"] = "Update Fail!";
            return View(collection);
        }
    }
}