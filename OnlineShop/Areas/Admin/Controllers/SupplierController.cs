﻿using ClassLibrary;
using Models;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class SupplierController : Controller
    {
        private SupplierModel SupplierModel = new SupplierModel();

        // GET: Admin/News
        public ActionResult Index()
        {
            return View(SupplierModel.ListAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Supplier collection)
        {
            if (SupplierModel.Create(collection) == 1)
            {
                TempData["SuccesStatus"] = "Create Success!";
                return RedirectToAction("Create");
            }
            TempData["ErrorStatus"] = "Create Fail!";
            return View(collection);
        }

        public ActionResult Delete(int ID)
        {
            if (SupplierModel.Delete(ID) == 1)
            {
                TempData["SuccessStatus"] = "Delete Success!";
                return RedirectToAction("Index");
            }
            TempData["ErrorStatus"] = "Delete Fail!";
            return View();
        }

        public ActionResult Edit(int ID)
        {
            var ViewDetail = SupplierModel.ViewDetail(ID);
            return View(ViewDetail);
        }

        [Authorize]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Supplier collection, int ID)
        {
            if (SupplierModel.Edit(collection, ID) == 1)
            {
                TempData["SuccesStatus"] = "Update Success!";
                return RedirectToAction("Edit");
            }
            TempData["ErrorStatus"] = "Update Fail!";
            return View(collection);
        }
    }
}