﻿using ClassLibrary;
using Models;
using Models.Framework;
using OnlineShop.Areas.Admin.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class GroupNewsController : Controller
    {
        private GroupNewsModel GroupNews = new GroupNewsModel();

        [Authorize]
        public ActionResult Index()
        {
            return View(GroupNews.List());
        }

        [Authorize]
        public ActionResult Create()
        {
            ViewBag.ParentID = GroupNews.List();
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GroupNew collection)
        {
            ViewBag.ParentID = GroupNews.List();
            collection.CreatedBy = collection.ModifiedBy = CommonFunction.GetSession("AccountLogin");
            if (ModelState.IsValid && GroupNews.Create(collection) == 1)
            {
                TempData["SuccesStatus"] = "Create Success!";
                return RedirectToAction("Create");
            }
            TempData["ErrorStatus"] = "Create Fail!";
            return View(collection);
        }

        [Authorize]
        public ActionResult Edit(int ID)
        {
            ViewBag.ParentID = GroupNews.List();
            return View(GroupNews.ViewDetail(ID));
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GroupNew collection, int ID)
        {
            ViewBag.ParentID = GroupNews.List();
            collection.ModifiedBy = CommonFunction.GetSession("AccountLogin");
            if (ModelState.IsValid && GroupNews.Edit(collection, ID) == 1)
            {
                TempData["SuccesStatus"] = "Update Success!";
                return RedirectToAction("Edit");
            }
            TempData["ErrorStatus"] = "Update Fail!";
            return View(collection);
        }

        [Authorize]
        public ActionResult Delete(int ID)
        {
            if (GroupNews.Delete(ID) == 1)
            {
                TempData["SuccessStatus"] = "Delete Success!";
                return RedirectToAction("Index");
            }
            TempData["ErrorStatus"] = "Delete Fail!";
            return View();
        }
    }
}