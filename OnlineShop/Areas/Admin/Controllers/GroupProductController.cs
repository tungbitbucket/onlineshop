﻿using ClassLibrary;
using Models;
using Models.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class GroupProductController : Controller
    {
        private GroupProductsModel GroupProductModel = new GroupProductsModel();

        [Authorize]
        public ActionResult Index()
        {
            return View(GroupProductModel.List());
        }

        [Authorize]
        public ActionResult Create()
        {
            ViewBag.ParentID = GroupProductModel.List();
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GroupProduct collection)
        {
            ViewBag.ParentID = GroupProductModel.List();
            collection.CreatedBy = collection.ModifiedBy = CommonFunction.GetSession("AccountLogin");
            if (ModelState.IsValid && GroupProductModel.Create(collection) == 1)
            {
                TempData["SuccesStatus"] = "Create Success!";
                return RedirectToAction("Create");
            }
            TempData["ErrorStatus"] = "Create Fail!";
            return View(collection);
        }

        [Authorize]
        public ActionResult Edit(int ID)
        {
            ViewBag.ParentID = GroupProductModel.List();
            return View(GroupProductModel.ViewDetail(ID));
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GroupProduct collection, int ID)
        {
            ViewBag.ParentID = GroupProductModel.List();
            collection.ModifiedBy = CommonFunction.GetSession("AccountLogin");
            if (ModelState.IsValid && GroupProductModel.Edit(collection, ID) == 1)
            {
                TempData["SuccesStatus"] = "Update Success!";
                return RedirectToAction("Edit");
            }
            TempData["ErrorStatus"] = "Update Fail!";
            return View(collection);
        }

        [Authorize]
        public ActionResult Delete(int ID)
        {
            if (GroupProductModel.Delete(ID) == 1)
            {
                TempData["SuccessStatus"] = "Delete Success!";
                return RedirectToAction("Index");
            }
            TempData["ErrorStatus"] = "Delete Fail!";
            return View();
        }
    }
}