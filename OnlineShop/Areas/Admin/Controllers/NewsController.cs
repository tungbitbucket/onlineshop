﻿using ClassLibrary;
using Models;
using Models.Framework;
using OnlineShop.Areas.Admin.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class NewsController : Controller
    {
        private NewsModel NewModel = new NewsModel();
        private GroupNewsModel GroupNewsModel = new GroupNewsModel();

        // GET: Admin/News
        public ActionResult Index()
        {
            return View(NewModel.ListAll());
        }

        public ActionResult Create()
        {
            ViewBag.GroupNewsList = GroupNewsModel.List();
            return View();
        }

        [Authorize]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(News collection)
        {
            collection.GroupNewsID = string.Join(",", collection.ArrayGroupNewID);
            collection.CreatedBy = CommonFunction.GetSession("AccountLogin");
            if (NewModel.Create(collection) == 1)
            {
                TempData["SuccesStatus"] = "Create Success!";
                return RedirectToAction("Create");
            }
            TempData["ErrorStatus"] = "Create Fail!";
            return View(collection);
        }

        public ActionResult Delete(int ID)
        {
            if (NewModel.Delete(ID) == 1)
            {
                TempData["SuccessStatus"] = "Delete Success!";
                return RedirectToAction("Index");
            }
            TempData["ErrorStatus"] = "Delete Fail!";
            return View();
        }

        //[HttpPost, ValidateInput(false)]
        //public JsonResult CreateNews(dynamic GroupNewsIDs, string GroupNewsTitle, string GroupNewsContent)
        //{
        //    try
        //    {
        //        string CreatedBy = CommonFunction.GetSession("AccountLogin");
        //        int NewsID = NewModel.Create(GroupNewsTitle, GroupNewsContent, CreatedBy);
        //        TempData["SuccesStatus"] = "Create Success!";
        //        return Json(new { status = 0, mes = "Success" });
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["ErrorStatus"] = "Create Fail!";
        //        return Json(new { status = 1, mes = ex.Message });
        //    }
        //}

        public ActionResult Edit(int ID)
        {
            var ViewDetail = NewModel.ViewDetail(ID);
            ViewBag.GroupNewsList = GroupNewsModel.List();
            return View(ViewDetail);
        }

        [Authorize]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(News collection, int ID)
        {
            collection.GroupNewsID = string.Join(",", collection.ArrayGroupNewID);
            collection.ModifiedBy = CommonFunction.GetSession("AccountLogin");
            if (NewModel.Edit(collection, ID) == 1)
            {
                TempData["SuccesStatus"] = "Update Success!";
                return RedirectToAction("Edit");
            }
            TempData["ErrorStatus"] = "Update Fail!";
            return View(collection);
        }

    }
}