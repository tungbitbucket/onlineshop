﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShop.Areas.Website.Controllers
{
    public class HomeController : Controller
    {
        // GET: Website/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}