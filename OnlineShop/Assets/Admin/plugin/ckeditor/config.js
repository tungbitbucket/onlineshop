/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	config.syntaxhightlight_lang = 'csharp';

	config.syntaxhightlight_hideControls = true;

	config.lang = 'vi';

	config.filebrowserBrowseUrl = '/Assets/Admin/plugin/ckfinder/ckfinder.html';

	config.filebrowserImageBrowseUrl = '/Assets/Admin/plugin/ckfinder/ckfinder.html?type=Images';

	config.filebrowserFlashBrowseUrl = '/Assets/Admin/plugin/ckfinder/ckfinder.html?type=Flash';

	config.filebrowserUploadUrl = '/Assets/Admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';

	config.filebrowserImageUploadUrl = '/Assets/Admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

	config.filebrowserFlashUploadUrl = '/Assets/Admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

	CKFinder.setupCKEditor(null, '/Assets/Admin/plugin/ckfinder/');
};
